# Adiczion's Modules for Odoo's Apps

You can find here the Adiczion modules published on [Odoo Apps](https://apps.odoo.com/apps/modules/browse?search=adiczion&price=Free)

## Licencing

All our developments are under license AGPL-v3, in the top level of this this repository you will find the full copyright notices and license terms.

## Modules

* **Common Logging (a4o_common_logging)**: Provides to other modules a centralization environment where they can drop their logging informations (typically when performing background action).

