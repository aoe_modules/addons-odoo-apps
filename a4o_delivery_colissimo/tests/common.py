# This file is part of an Adiczion's Module.
# The COPYRIGHT and LICENSE files at the top level of this repository
# contains the full copyright notices and license terms.
from odoo.addons.base.tests.common import TransactionCase
import os


class TestColissimoLogin(TransactionCase):
    ''' Add environment variables for carrier logins when launching colissimo
    tests, e.g.:
        CARRIER_USER=123456 CARRIER_PWD=password ./base/odoo-bin -c test.conf
        -i a4o_delivery_colissimo --test-tags /a4o_delivery_colissimo
        -d db_test --stop-after-init
    '''
    @classmethod
    def setUpClass(cls):
        super(TestColissimoLogin, cls).setUpClass()

        cls.coli_login = os.environ.get('CARRIER_USER')
        cls.coli_password = os.environ.get('CARRIER_PWD')


class TestColissimoCommonBase(TransactionCase):
    ''' Setup with sale test configuration. '''
    @classmethod
    def setUpClass(cls):
        super(TestColissimoCommonBase, cls).setUpClass()

        cls.url_base = "https://ws.colissimo.fr"

        cls.company_data = {
            'shipping_url': (
                "%s/sls-ws/SlsServiceWS?wsdl" % cls.url_base),
            'relaypoint_url': (
                "%s/pointretrait-ws-cxf/PointRetraitServiceWS/2.0?wsdl"
                % cls.url_base),
            'default_pricelist': cls.env['product.pricelist'].create({
                'name': 'default_pricelist',
                'currency_id': cls.env.company.currency_id.id,
            }),
            'product_category': cls.env['product.category'].create({
                'name': 'Test category',
            }),
        }

        cls.sender = cls.env['res.partner'].create({
            'name': 'Adiczion',
            'street': '1 avenue Louis COIRARD',
            'street2': 'Rés. les Hespérides',
            'city': 'Aix en Provence',
            'zip': '13090',
            'country_id': cls.env['res.country'].search([
                ('code', '=', 'FR')]).id,
            'email': 'contact@adiczion.com',
            'phone': '+33950693113',
            'company_id': False,
            })
        cls.europe_customer = cls.env['res.partner'].create({
            'name': 'Customer in Belgium',
            'street': 'Jean Joseph Piret 45',
            'city': 'Joncret',
            'zip': '6280',
            'country_id': cls.env['res.country'].search([
                ('code', '=', 'BE')]).id,
            'email': 'josephp@customer.com',
            'company_id': False,
            })
        cls.france_customer = cls.env['res.partner'].create({
            'name': 'Adiczion (desk)',
            'street': '8 rue Jean DARET',
            'street2': 'Bât 10',
            'city': 'Aix en provence',
            'zip': '13090',
            'country_id': cls.env['res.country'].search([
                ('code', '=', 'FR')]).id,
            'email': 'contact_desk@adiczion.com',
            'phone': '+33950693113',
            'mobile': '+33600693583',
            'company_id': False,
            })
        cls.package = cls.env['product.packaging'].search([
                ('name', '=', 'Chronopost Custom Parcel'),
                ])

        cls.product = cls.env['product.product'].create({
            'name': 'Product to order',
            'categ_id': cls.company_data['product_category'].id,
            'standard_price': 235.0,
            'list_price': 280.0,
            'type': 'consu',
            'weight': 0.01,
            'uom_id': cls.env.ref('uom.product_uom_unit').id,
            'uom_po_id': cls.env.ref('uom.product_uom_unit').id,
            'default_code': 'FURN_9999',
            'invoice_policy': 'order',
            'expense_policy': 'no',
            'taxes_id': [(6, 0, [])],
            'supplier_taxes_id': [(6, 0, [])],
            })


class TestColissimoCommon(TestColissimoCommonBase, TestColissimoLogin):
    ''' Setup with colissimo test configuration. '''
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        cls.company_data.update({
            'account_number': cls.coli_login,
            'account_passwd': cls.coli_password,
        })
