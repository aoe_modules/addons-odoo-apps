# -*- coding: utf-8 -*-
# This file is part of an Adiczion's Module.
# The COPYRIGHT and LICENSE files at the top level of this repository
# contains the full copyright notices and license terms.
from odoo.tests import tagged
from .common import TestColissimoCommon


@tagged('post_install', '-at_install')
class TestColissimo(TestColissimoCommon):
    ''' Add environment variables for carrier logins when launching colissimo
    tests, e.g.:
        CARRIER_USER=123456 CARRIER_PWD=password ./base/odoo-bin -c test.conf
        -i a4o_delivery_colissimo --test-tags /a4o_delivery_colissimo
        -d db_test --stop-after-init
    '''
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        cls.delivery_method = cls.env['delivery.carrier'].create({
            'name': "Colissimo (w/o signature)",
            'product_id': cls.env['product.product'].search([
                ('default_code', '=', 'colissimo01'),
                ])[0].id,
            'delivery_type': "colissimo",
            'coli_service_type': "wo_signature",
            'sender_id': cls.sender.id,
            # Test login proposed by the Colissimo API
            'coli_account_number': cls.company_data['account_number'],
            'coli_passwd': cls.company_data['account_passwd'],
            'coli_shipping_url': cls.company_data['shipping_url'],
            'coli_relaypoint_url': cls.company_data['relaypoint_url'],
        })

        SaleOrder = cls.env['sale.order'].with_context(tracking_disable=True)
        # create a generic Sale Order with all classical products
        cls.sale_order = SaleOrder.create({
            'partner_id': cls.france_customer.id,
            'partner_invoice_id': cls.france_customer.id,
            'partner_shipping_id': cls.france_customer.id,
            'pricelist_id': cls.company_data['default_pricelist'].id,
        })
        cls.sale_order_line_1 = cls.env['sale.order.line'].create({
            'name': cls.product.name,
            'product_id': cls.product.id,
            'product_uom_qty': 2,
            'product_uom': cls.product.uom_id.id,
            'price_unit': cls.product.list_price,
            'order_id': cls.sale_order.id,
            'tax_id': False,
        })

    def test_shipping(self):
        """ Test the flow of sales orders through to shipment with
            the colissimo carrier
        """
        self.sale_order.order_line.read(
            ['name', 'price_unit', 'product_uom_qty', 'price_total'])
        self.sale_order.carrier_id = self.delivery_method.id
        self.assertEqual(self.sale_order.amount_total,
            560.0, 'Sale: total amount is wrong')

        # confirm quotation
        self.sale_order.action_confirm()
        self.assertTrue(self.sale_order.picking_ids,
            'Sale Stock: no picking created for "invoice on delivery"'
                'storable products')
        self.assertTrue(self.sale_order.state == 'sale')
        self.assertTrue(self.sale_order.invoice_status == 'to invoice')

        # order delivery
        self.assertEqual(self.sale_order.delivery_count,
            1.0, 'Delivery: the number of deliveries is wrong')
        self.assertEqual(self.sale_order.picking_ids.carrier_id.name,
            'Colissimo (w/o signature)',
            'Delivery: the delivery carrier is wrong')
        self.assertTrue(self.sale_order.picking_ids.state == 'assigned')

        # pack the order
        pack = self.sale_order.picking_ids.action_put_in_pack()
        wiz_pack = self.env['choose.delivery.package'].with_context(
            pack['context']).create({})
        wiz_pack.shipping_weight = 5.0
        wiz_pack.delivery_package_type_id = self.package
        wiz_pack.action_put_in_pack()

        self.assertEqual(self.sale_order.picking_ids.shipping_weight, 5.0)
        self.assertTrue(self.sale_order.picking_ids.package_ids,
            'Picking: no packages ready to this shipment')
        self.assertEqual(len(self.sale_order.picking_ids.package_ids),
            1.0, 'Picking: number of package made is wrong')

        # validate the delivery
        self.sale_order.picking_ids.button_validate()
        self.assertTrue(self.sale_order.picking_ids.state == 'done')
        self.assertTrue(self.sale_order.picking_ids.carrier_tracking_ref,
            'Tracking number: No tracking number for this shipment')
        print(self.sale_order.picking_ids.carrier_tracking_ref)
